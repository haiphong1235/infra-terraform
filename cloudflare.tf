resource "cloudflare_record" "prodfe" {
  zone_id = "${var.cf_zone_id}"
  name    = "prodwebapp"
  value   = "${aws_eip.web_ip["prod"].public_ip}"
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "prodbe" {
  zone_id = "${var.cf_zone_id}"
  name    = "prodwebapi5"
  value   = "${aws_eip.web_ip["prod"].public_ip}"
  type    = "A"
  proxied = false
}
resource "cloudflare_record" "devfe" {
  zone_id = "${var.cf_zone_id}"
  name    = "devwebapp"
  value   = "${aws_eip.web_ip["dev"].public_ip}"
  type    = "A"
  proxied = false
}
resource "cloudflare_record" "devbe" {
  zone_id = "${var.cf_zone_id}"
  name    = "devwebapi5"
  value   = "${aws_eip.web_ip["dev"].public_ip}"
  type    = "A"
  proxied = false
}