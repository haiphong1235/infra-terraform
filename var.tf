variable "ingress" {
  type    = list(number)
  default = [80, 443, 22]
}
variable "domain" {
  default = "tntstore24.com"
}
variable "email" {
}
variable "token" {
}
variable "region" {
}

variable "cf_zone_id" {
  type    = string
  default = "e3e5f7e39fe85b2fa1c9ba67a26505ce"
}

variable "environments"{
  type = list(string)
  default = ["prod", "dev"]
}
variable "datadog_api_key"{
}
variable "datadog_app_key"{}