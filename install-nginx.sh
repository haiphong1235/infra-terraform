#! /bin/bash
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

#install nginx

sudo apt-get update

sudo apt-get install nginx -y
sudo apt-get install unzip -y
sudo apt update
sudo apt install default-jre -y
cd /home/ubuntu
mkdir front-end
mkdir back-end
mkdir jenkins
sudo chmod -R 777 /home/ubuntu/front-end
sudo chmod -R 777 /home/ubuntu/back-end
sudo chmod -R 777 /home/ubuntu/jenkins

sudo chmod -R 777 /etc/nginx/sites-enabled
sudo rm /etc/nginx/sites-enabled/default
sudo snap install core; sudo snap refresh core
# sudo apt install -y certbot python3-certbot-nginx
sudo mkdir /certtest
sudo chmod -R 777 /certtest
# sudo openssl genrsa -out ca.key 2048
# sudo openssl req -new -key ca.key -out ca.csr -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
# sudo openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt