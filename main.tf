provider "aws" {
  region = "${var.region}"
}
provider "cloudflare" {
  email   = "${var.email}"
  api_key = "${var.token}"
}
provider "datadog"{

  api_key = "${var.datadog_api_key}"
  app_key = "${var.datadog_app_key}"

}
resource "aws_default_vpc" "MyVPC"{
  tags = {
    Name = "Default VPC"
  }
}
resource "aws_key_pair" "aws" {
  key_name   = "aws"
  public_key = file("./aws-sshkey/ssh-key-aws/aws.pub")
}


# resource "aws_eip_association" "eip_assoc" {
#   instance_id   = aws_instance.production.id
#   allocation_id = aws_eip.web_ip.id
# }
resource "aws_eip" "web_ip" {
      for_each = toset(var.environments)
      instance = aws_instance.webserver[each.value].id
}


resource "aws_instance" "webserver" {
  for_each = toset(var.environments)
  ami             = "${data.aws_ami.latest-ubuntu.id}"
  instance_type   = "t2.micro"
  /* security_groups = [aws_security_group.web_traffic.name] */
  vpc_security_group_ids = [aws_security_group.web_traffic.id]
  key_name        = "${aws_key_pair.aws.key_name}"
  tags = {
    Name = "${each.value}-server"
  }
  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    source      = "install-nginx.sh"
    destination = "./install-nginx.sh"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    source      = "install-dotnet.sh"
    destination = "./install-dotnet.sh"
  }


  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    source      = "webapp"
    destination = "./webapp"
  }
  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    source      = "webapi"
    destination = "./webapi"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    source      = "quickapp.service"
    destination = "./quickapp.service"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("./aws-sshkey/ssh-key-aws/aws")
      timeout     = "2m"
    }
    inline = [
      "sudo chmod +x ./install-nginx.sh",
      "sudo ./install-nginx.sh",
      "sudo chmod +x ./install-dotnet.sh",
      "sudo ./install-dotnet.sh",
      "sudo mv ./quickapp.service /etc/systemd/system/",
      "sudo systemctl start quickapp",
      "sudo systemctl enable quickapp",
      "sudo dotnet dev-certs https --clean",
      "sudo dotnet dev-certs https --verbose",
      "sudo mv webapp /etc/nginx/sites-enabled/webapp",
      "sudo mv webapi /etc/nginx/sites-enabled/webapi",
      

    ]
  }


}


resource "aws_security_group" "web_traffic" {
  name = "Allow-web-traffic"
  vpc_id = "${aws_default_vpc.MyVPC.id}"
  dynamic "ingress" {
    iterator = port
    for_each = var.ingress
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
resource "null_resource" "certbot"{
    provisioner "file" {
      connection {
        type        = "ssh"
        user        = "ubuntu"
        host        = aws_eip.web_ip["prod"].public_ip
        private_key = file("./aws-sshkey/ssh-key-aws/aws")
        timeout     = "2m"
      }
      source      = "prodwebapi5.tntstore24.com/"
      destination = "/certtest"
     
    }

    provisioner "remote-exec" {
      connection {
        type        = "ssh"
        user        = "ubuntu"
        host        = aws_eip.web_ip["prod"].public_ip
        private_key = file("./aws-sshkey/ssh-key-aws/aws")
        timeout     = "2m"
      }
      inline = [
        "sed -i \"s@server_name _@server_name prodwebapp.tntstore24.com@g\" /etc/nginx/sites-enabled/webapp ",
        "sed -i \"s@server_name _@server_name prodwebapi5.tntstore24.com@g\" /etc/nginx/sites-enabled/webapi ",
        "sed -i \"s@proxy_pass _@proxy_pass https://localhost:8001@g\" /etc/nginx/sites-enabled/webapi",
        "sudo service nginx enable",
        "sudo service nginx start",
        "sudo service nginx restart"
        /* "sudo certbot --nginx -n -d prodwebapi5.tntstore24.com --email haiphong1235@gmail.com --agree-tos --redirect --hsts", */
        
      ]
    }
    
    provisioner "file" {
      connection {
        type        = "ssh"
        user        = "ubuntu"
        host        = aws_eip.web_ip["dev"].public_ip
        private_key = file("./aws-sshkey/ssh-key-aws/aws")
        timeout     = "2m"
      }
      source      = "devwebapi5.tntstore24.com/"
      destination = "/certtest"
     
    }
    provisioner "remote-exec" {
      connection {
        type        = "ssh"
        user        = "ubuntu"
        host        = aws_eip.web_ip["dev"].public_ip
        private_key = file("./aws-sshkey/ssh-key-aws/aws")
        timeout     = "2m"
      }
      inline = [
        "sed -i \"s@server_name _@server_name devwebapp.tntstore24.com@g\" /etc/nginx/sites-enabled/webapp ",
        "sed -i \"s@server_name _@server_name devwebapi5.tntstore24.com@g\" /etc/nginx/sites-enabled/webapi ",
        "sed -i \"s@proxy_pass _@proxy_pass https://localhost:6001@g\" /etc/nginx/sites-enabled/webapi ",
        "sudo service nginx enable",
        "sudo service nginx start",
        "sudo service nginx restart"
        /* "sudo certbot --nginx -n -d devwebapi5.tntstore24.com --email haiphong1235@gmail.com --agree-tos --redirect --hsts", */
      ]
    }
}
resource "datadog_monitor" "process_alert_example" {
  name    = "Network outbound alert"
  type    = "metric alert"
  message = "Network outbound alert running on. Notify: @slack-phongdepzai-quickapp_prod"
  query = "avg(last_1m):avg:aws.ec2.network_out{instance_id:${aws_instance.webserver["prod"].id}} > 100"
 
  monitor_thresholds {
    critical = 100
    critical_recovery = 0.5
  }
  evaluation_delay = 900
  require_full_window = false
  notify_no_data = false
  renotify_interval = 15
 
  notify_audit = false
  timeout_h = 60
  include_tags = true
 
 # ignore any changes in silenced value; using silenced is deprecated in favor of downtimes
  lifecycle {
    ignore_changes = [silenced]
  }
}
output "WebProd_PublicIP" {
  value = aws_eip.web_ip["prod"].public_ip
}
output "WebDev_PublicIP" {
  value = aws_eip.web_ip["dev"].public_ip
}
data "aws_ami" "latest-ubuntu" {
  most_recent = true
  owners = [ "099720109477" ]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }
}



